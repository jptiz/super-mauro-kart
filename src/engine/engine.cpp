#include "engine.h"

#include <iostream>
#include <string>

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

namespace options {

using namespace std::string_literals;

constexpr auto WIDTH = 800;
constexpr auto HEIGHT = 600;

const auto TITLE = "Super Mauro Kart!"s;

}

using namespace sf;

namespace {

auto next_event(sf::RenderWindow& window) {
    auto event = sf::Event{};
    return window.pollEvent(event)
        ? std::optional{event}
        : std::nullopt;
}

void opengl_prepare() {
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(true);
    glClearDepth(1);
    glClearColor(0.f, 0.f, .3f, 1.f);
}

void draw_axes() {
    glBegin(GL_LINES);
        glColor3f(1, 1, 1);
        glVertex3f(-0.5, 0, 0);
        glVertex3f(+0.5, 0, 0);
        glVertex3f(+0.4, -0.05, 0);
        glVertex3f(+0.4, +0.05, 0);
        glVertex3f(+0.5, 0, 0);
    glEnd();
    glBegin(GL_LINES);
        glColor3f(1, 1, 1);
        glVertex3f(0, -0.5, 0);
        glVertex3f(0, +0.5, 0);
    glEnd();
    glBegin(GL_LINES);
        glColor3f(1, 1, 1);
        glVertex3f(0, 0, -0.5);
        glVertex3f(0, 0, +0.5);
    glEnd();
}

}

class MapState: public engine::GameState {
public:
    MapState() {
        track.loadFromFile("res/battletrack1.png");
    }

    void draw() override {
        render();
        render_map();
        draw_axes();
    }

    void on_key_press(Keyboard::Key key) override {
        keys[key] = true;
    }

    void on_key_release(Keyboard::Key key) override {
        keys[key] = false;
    }

    void update() override {
        if (keys[Keyboard::Key::W]) {
            --angle.x;
        } else if (keys[Keyboard::Key::S]) {
            ++angle.x;
        }

        if (keys[Keyboard::Key::Q]) {
            --angle.y;
        } else if (keys[Keyboard::Key::E]) {
            ++angle.y;
        }

        if (keys[Keyboard::Key::A]) {
            --angle.z;
        } else if (keys[Keyboard::Key::D]) {
            ++angle.z;
        }

        if (keys[Keyboard::Key::Up]) {
            --z;
        } else if (keys[Keyboard::Key::Down]) {
            ++z;
        }
    }

    void render() {
        glTranslated(0, 0, z / 4.f);

        glRotated(angle.x, 8, 0, 0);
        glRotated(angle.y, 0, 8, 0);
        glRotated(angle.z, 0, 0, 8);
    }

    void render_map() {
        Texture::bind(&track);
        glBegin(GL_QUADS);
        glTexCoord3f(0.f, 0.f, 0.f);
        glVertex3f(-1.f, -1.f,  0.f);

        glTexCoord3f(1.f, 0.f, 0.f);
        glVertex3f(1.f, -1.f,  0.f);

        glTexCoord3f(1.f, 1.f, 0.f);
        glVertex3f(1.f, 1.f,  0.f);

        glTexCoord3f(0.f, 1.f, 0.f);
        glVertex3f(-1.f, 1.f,  0.f);
        glEnd();
    }

private:
    Vector3f angle;
    float z;
    Texture track;
    std::map<Keyboard::Key, bool> keys;
};

// Members only!
namespace engine {

GameWindow::GameWindow():
    window{
        VideoMode{options::WIDTH, options::HEIGHT},
        options::TITLE,
    },
    state{new MapState{}}
{
    window.setVerticalSyncEnabled(true);
    window.setKeyRepeatEnabled(false);
    window.setActive();
    window.setFramerateLimit(60);
}

void GameWindow::main_loop() {
    while (window.isOpen()) {
        while (auto _event = next_event(window)) {
            auto event = *_event;
            if (event.type == sf::Event::Closed) {
                window.close();
            } else if (event.type == Event::KeyPressed) {
                if (state) {
                    state->on_key_press(event.key.code);
                }
            } else if (event.type == Event::KeyReleased) {
                if (state) {
                    state->on_key_release(event.key.code);
                }
            } else if (event.type == Event::Resized) {
                on_resize(event.size);
            }
        }

        if (state) {
            state->update();
        }

        window.clear();

        window.pushGLStates();

        glLoadIdentity();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        if (state) {
            state->draw();
        }

        window.popGLStates();

        window.display();
    }
}

void GameWindow::on_resize(const Event::SizeEvent& size) {
    auto [width, height] = size;
    auto ratio = (int)width / height;

    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();
    glFrustum(0.5*ratio, 0.5*ratio, -0.5*ratio, .5*ratio, 0, 500);

    glMatrixMode(GL_MODELVIEW);
}

void GameWindow::show() {
    opengl_prepare();

    main_loop();
}

void GameWindow::close() {
    window.close();
}

}
