#include <iostream>

#include "engine.h"

int main() {
    std::cout << "works!\n";
    auto window = engine::GameWindow{};
    window.show();
}
