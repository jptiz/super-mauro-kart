Super Mauro Kart
================

Sobre
-----

Primeiro: não sei o quão público isto aqui deveria ser, mas, enfim, não é para
maus-fins. É só porque brincadeiras com nomes são legais. No fundo no fundo
todos amam o titio-Mauro.


Compilar
--------

Compilar pela primeira vez:

```bash
mkdir build
cd build
cmake ../src
make
```

Depois disso, basta chamar `make` dentro de `build/` quando for recompilar.


TO-DO'S
-------

1. Criar janelinha com SFML;
2. Exibir mapa 2D à la Mode-7 da maneira mais porca possível;
3. Jogar um player movível no meio;
4. Criar sistema de colisão com paredes;
5. Por aí vai;
6. Isso aqui vai longe (espero que não muito).
